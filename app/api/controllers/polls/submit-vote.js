module.exports = {


  friendlyName: 'Submit Vote',


  description: 'Submit the given vote information',



  inputs: {


    candidate: {
      description: 'Candidate to vote for',
      type: 'string',
      required: true
    },

    election: {
      description: 'Election being voted on',
      type: 'string',
      required: true
    }

  },


  exits: {
  
    success: {
      viewTemplatePath: 'pages/polls/open-polls',
    },

    redirect: {
      description: 'The requesting user is not logged in, or the poll isn\'t found',
      responseType: 'redirect'
    }

  },


  fn: async function (inputs, exits) {
    // fail when not logged in - redirect to log in
    if (!this.req.me) {
      throw { redirect: '/login' };
    }

    // check if the poll fails, redirect to poll not found page if not present
    try {
      //only make a vote if we have not yet voted
      if(!await Vote.count({
        where: {user: this.req.me.id,
                election: inputs.election}
      })) {
        await Vote.create({ election: inputs.election, candidate: inputs.candidate, user: this.req.me.id})
      }
      
      return this.res.redirect('/polls/open')

    } catch (err) {
      throw { redirect: '/polls/not_found' };
    }
  }


};
