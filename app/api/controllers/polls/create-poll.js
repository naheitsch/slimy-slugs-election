module.exports = {


    friendlyName: 'View an open poll',
  
  
    description: 'Displays an open poll',
  
    inputs: {
    },

    exits: {
  
      success: {
        viewTemplatePath: 'pages/polls/create-poll',
      },

      redirect: {
        description: 'The requesting user is not logged in, or the poll isn\'t found',
        responseType: 'redirect'
      }
  
    },
  
  
    fn: async function (inputs, exits) {
      // fail when not logged in - redirect to log in
        if (!this.req.me) {
            throw { redirect: '/login' };
        }
        if(!this.req.me.isSuperAdmin) {
            throw {redirect: '/'};
        }

        // check if the poll fails, redirect to poll not found page if not present
        const currentCandidates = await Candidate.find({})
        return exits.success({candidates: currentCandidates}); 
    }
  
  
  };
