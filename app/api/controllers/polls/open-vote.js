module.exports = {


    friendlyName: 'View an open poll',
  
  
    description: 'Displays an open poll',
  
    inputs: {
        id: {
          description: 'The name of the open poll to look at.',
          type: 'string',
          required: true
        }
    },

    exits: {
  
      success: {
        viewTemplatePath: 'pages/polls/open-vote',
      },

      redirect: {
        description: 'The requesting user is not logged in, or the poll isn\'t found',
        responseType: 'redirect'
      }
  
    },
  
  
    fn: async function (inputs, exits) {
      // fail when not logged in - redirect to log in
      if (!this.req.me) {
        throw { redirect: '/login' };
      }

      // check if the poll fails, redirect to poll not found page if not present
      try {
        // throws exception if there isn't one
        const pollInfo = await Election.find({ id: inputs.id }).limit(1).populate('candidates');
        const voted = await Vote.count({user: this.req.me.id, election: inputs.id});
        return exits.success({ poll: pollInfo[0], hasVoted: voted}); // object with id and name, print the name for now

      } catch (err) {
        throw { redirect: '/polls/not_found' };
      }
    }
  
  
  };
