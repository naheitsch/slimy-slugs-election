module.exports = {


  friendlyName: 'View polls that have finished',


  description: 'Displays the polls that have closed.',


  exits: {

    success: {
      viewTemplatePath: 'pages/polls/closed-polls',
    }

  },


  fn: async function (inputs, exits) {
    let pollInfo;

    try {
      pollInfo = await Election.find({ isOpen: false }); // list of { id, name }
    } catch(err) {
      pollInfo = []; // no items in DB
    }

    const loggedIn = this.req.me;

    return exits.success({ results: pollInfo, isLoggedIn: loggedIn });
  }


};
