module.exports = {


    friendlyName: 'Finalization creation of new poll',
  
  
    description: 'Displays an open poll',
  
    inputs: {
        currentCandidates: {
            description: 'The name of the open poll to look at.',
            type: 'string',
            required: true
        },
        newCandidates: {
            description: 'The name of the open poll to look at.',
            type: 'string',
            required: true
          },
          electionName: {
            description: 'The name of the open poll to look at.',
            type: 'string',
            required: true
          }
    },

    exits: {
      success: {
        viewTemplatePath: 'pages/polls/open-vote',
      },

      redirect: {
        description: 'The requesting user is not logged in, or the poll isn\'t found',
        responseType: 'redirect'
      }
  
    },
  
  
    fn: async function (inputs, exits) {
      // fail when not logged in - redirect to log in
        if (!this.req.me) {
            throw { redirect: '/login' };
        }
        if(!this.req.me.isSuperAdmin) {
            throw {redirect: '/'};
        }
        let currentCandidates = Object.keys(JSON.parse(inputs.currentCandidates));
        let newCandidates = JSON.parse(inputs.newCandidates);
        let newCandidateNames = Object.keys(newCandidates);
        let electionName = inputs.electionName;
        if(currentCandidates.length + newCandidateNames.length < 2) {
          throw {redirect: '/'}
        }
        for(let i=0;i<newCandidateNames.length;i++) {
          let name = newCandidateNames[i];
          let bio = newCandidates[name];
          let newCandidate = await Candidate.create({fullName: name, bio: bio}).fetch();
          currentCandidates.push(newCandidate.id);
        }
        var link = '/'
        await new Promise(function(resolve, reject) {
          Election.findOrCreate({ name: electionName }, {name: electionName, isOpen: true})
          .exec(async(err, election, wasCreated)=> {
            if (err) { 
              resolve();
            }
            if(wasCreated) {
              await Election.addToCollection(election.id, 'candidates').members(currentCandidates);
              link = '/polls/open/' + election.id; 
              resolve();
            }
            else {
              //poll was no created because name was already in use
              resolve();
            }
          });
        });
        return this.res.redirect(link);
    }
  };
