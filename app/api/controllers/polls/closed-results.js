module.exports = {


    friendlyName: 'View a specific poll that has finished',
  
  
    description: 'Displays a specific poll that has closed.',
  
    inputs: {
      id: {
        description: 'The id of the closed poll to look at.',
        type: 'string',
        required: true
      }
    },
  
    exits: {
  
      success: {
        viewTemplatePath: 'pages/polls/closed-results',
      },

      redirect: {
        description: 'The poll isn\'t found',
        responseType: 'redirect'
      }
  
    },
  
  
    fn: async function (inputs, exits) {
      // TODO fail if the pollName is not a real poll in our database
      // check if the poll fails, redirect to poll not found page if not present
      try {
        // throws exception if there isn't one

        // ideally do a nested populate on the
        const pollInfo = await Election.findOne({ id: inputs.id }).populate('votes');

        // verify the poll is in fact closed.
        //  if it isn't, then the user typed in the url, since there wouldn't be the link to this page.
        //  although it exists, we're telling them this page doesn't exist. (there's no results yet).
        if (pollInfo.isOpen) {
          throw { redirect: '/polls/not_found' };
        }

        let userIdVotedList = []; // list of user id's

        let votesByCandidate = {}; // map of candidate id -> vote count.

        // for security reasons, we don't want to expose the association to the view -- if there's a crash
        //   it might leak the association of user -> vote in the debug info.
        // do the tallies here, and then pass the processed data to the view.

        const votes = pollInfo.votes;
        for (let i = 0; i < votes.length; i++) {
          let vote = votes[i]; // election, candidate, user properties

          // added to list of users that voted
          userIdVotedList.push(vote.user);

          // tally the vote for the candidate
          let candidateId = vote.candidate;
          if (!votesByCandidate[candidateId]) {
            votesByCandidate[candidateId] = 0;
          }
          votesByCandidate[candidateId] ++;
        }

        // find the maximum of the userIdVotedList
        let maxVotes = 0;
        let winningCandidateIds = []; // allow support for ties, they will both be shown

        Object.entries(votesByCandidate).forEach(
          ([candidateId, voteCount]) => {
            // new winner, they alone have the highest
            if (voteCount > maxVotes) {
              winningCandidateIds = [candidateId];
              maxVotes = voteCount;
            } else if (voteCount === maxVotes) { // tie with the current best, represent both
              winningCandidateIds.push(candidateId);
            }
          }
        );

        // find the candidate names from the winningCandidates -- pass these to the view
        const winningCandidates = await Candidate.find().where({ id: winningCandidateIds });

        // figure out the users who voted & who didn't (technical requirement)
        // there is probably a query way to do this, but as far as I know that requires 2 queries.
        //  don't include the sensitive info, just need the id and fullName
        const allUsers = await User.find({ select: ['id', 'fullName']});

        // filter out into the categories, then map to just a list of names.
        const votedUsers = allUsers.filter(item => userIdVotedList.includes(item.id)).map(x => x.fullName);
        const nonVotedUsers = allUsers.filter(item => !userIdVotedList.includes(item.id)).map(x => x.fullName);

        // render the template with the data
        return exits.success({ name: pollInfo.name, winners: winningCandidates, votedUsers, nonVotedUsers });

      } catch (err) { // either thrown above, or there isn't a poll with the id.
        console.warn(err);
        throw { redirect: '/polls/not_found' };
      }
    }
  
  
  };
