module.exports = {


  friendlyName: 'View currently open polls',


  description: 'Displays the currently open polls',


  exits: {

    success: {
      viewTemplatePath: 'pages/polls/open-polls',
    }

  },


  fn: async function (inputs, exits) {

    let pollInfo;

    try {
      pollInfo = await Election.find({ isOpen: true }); // list of { id, name }
    } catch(err) {
      pollInfo = []; // no items in DB
    }
    
    const loggedIn = this.req.me;

    return exits.success({ results: pollInfo, isLoggedIn: loggedIn });
  }


};
