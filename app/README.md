# Web App

This Web Application is a secure voting platform that allows users to log in and vote on multiple open elections 
as well as view the results for closed elections.

The following folders are important:

`api` - The API folder holds the core functionality of the application. Inside you will find folders for the data 
Models, Controllers, and Policies. The Models are defined objects that are used to create the database as well as 
handle data throughout the application. The Controllers are what process the main actions of the application such 
as requesting a route or processing an API call and querying data from the database. The Policies folder contains 
security policies that help restrict and control access to actions throughout the application.

`assets` - The assets folder is where all of the static files for web delivery are found such as CSS, images, and 
Javascript files for the web. 

`config` - The config folder is where all the configuration files for the web application live. Most importantly 
is the `bootstrap.js` file which seeds some predefined test data into the database on first startup. This allows
 for constant test data to query from the database in order to test the application on the fly. 


### Links

+ [Sails framework documentation](https://sailsjs.com/get-started)
+ [Version notes / upgrading](https://sailsjs.com/documentation/upgrading)
+ [Deployment tips](https://sailsjs.com/documentation/concepts/deployment)
+ [Community support options](https://sailsjs.com/support)
+ [Professional / enterprise options](https://sailsjs.com/enterprise)
