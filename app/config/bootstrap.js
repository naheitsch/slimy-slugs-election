/**
 * Seed Function
 * (sails.config.bootstrap)
 *
 * A function that runs just before your Sails app gets lifted.
 * > Need more flexibility?  You can also create a hook.
 *
 * For more information on seeding your app with fake data, check out:
 * https://sailsjs.com/config/bootstrap
 */

module.exports.bootstrap = async function() {

    // Import dependencies
    var path = require('path');

    // This bootstrap version indicates what version of fake data we're dealing with here.
    var HARD_CODED_DATA_VERSION = 0;

    // This path indicates where to store/look for the JSON file that tracks the "last run bootstrap info"
    // locally on this development computer (if we happen to be on a development computer).
    var bootstrapLastRunInfoPath = path.resolve(sails.config.appPath, '.tmp/bootstrap-version.json');

    // Whether or not to continue doing the stuff in this file (i.e. wiping and regenerating data)
    // depends on some factors:
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    // If the hard-coded data version has been incremented, or we're being forced
    // (i.e. `--drop` or `--environment=test` was set), then run the meat of this
    // bootstrap script to wipe all existing data and rebuild hard-coded data.
    if (sails.config.models.migrate !== 'drop' && sails.config.environment !== 'test') {
        // If this is _actually_ a production environment (real or simulated), or we have
        // `migrate: safe` enabled, then prevent accidentally removing all data!
        if (process.env.NODE_ENV==='production' || sails.config.models.migrate === 'safe') {
            sails.log('Since we are running with migrate: \'safe\' and/or NODE_ENV=production (in the "'+sails.config.environment+'" Sails environment, to be precise), skipping the rest of the bootstrap to avoid data loss...');
            return;
        }//•

        // Compare bootstrap version from code base to the version that was last run
        var lastRunBootstrapInfo = await sails.helpers.fs.readJson(bootstrapLastRunInfoPath)
            .tolerate('doesNotExist');// (it's ok if the file doesn't exist yet-- just keep going.)

        if (lastRunBootstrapInfo && lastRunBootstrapInfo.lastRunVersion === HARD_CODED_DATA_VERSION) {
            sails.log('Skipping v'+HARD_CODED_DATA_VERSION+' bootstrap script...  (because it\'s already been run)');
            sails.log('(last run on this computer: @ '+(new Date(lastRunBootstrapInfo.lastRunAt))+')');
            return;
        }//•

        sails.log('Running v'+HARD_CODED_DATA_VERSION+' bootstrap script...  ('+(lastRunBootstrapInfo ? 'before this, the last time the bootstrap ran on this computer was for v'+lastRunBootstrapInfo.lastRunVersion+' @ '+(new Date(lastRunBootstrapInfo.lastRunAt)) : 'looks like this is the first time the bootstrap has run on this computer')+')');
    }
    else {
        sails.log('Running bootstrap script because it was forced...  (either `--drop` or `--environment=test` was used)');
    }

    // Since the hard-coded data version has been incremented, and we're running in
    // a "throwaway data" environment, delete all records from all models.
    for (let identity in sails.models) {
        await sails.models[identity].destroy({});
    }//∞

    // By convention, this is a good place to set up fake data during development.
    await User.createEach([
        { emailAddress: 'admin@example.com', license: '123456', ssn: '123121234', fullName: 'Ryan Dahl', isSuperAdmin: true, password: await sails.helpers.passwords.hashPassword('abc123') },
    ]);

    var users = await User.createEach([
        { emailAddress: 'johndoe@example.com', license: '418238', ssn: '234542345', fullName: 'John Doe', isSuperAdmin: false, password: await sails.helpers.passwords.hashPassword('abc123') },
        { emailAddress: 'freddoe@example.com', license: '418267', ssn: '434542345', fullName: 'Fred Doe', isSuperAdmin: false, password: await sails.helpers.passwords.hashPassword('abc123') },
        { emailAddress: 'markdoe@example.com', license: '418236', ssn: '334542345', fullName: 'Mark Doe', isSuperAdmin: false, password: await sails.helpers.passwords.hashPassword('abc123') },
        { emailAddress: 'stevedoe@example.com', license: '418235', ssn: '434542345', fullName: 'Steve Doe', isSuperAdmin: false, password: await sails.helpers.passwords.hashPassword('abc123') },
    ]).fetch();

    sails.log(users);

    // Create Candidates
    var candidates = await Candidate.createEach([
        { fullName: 'Theodore Roosevelt', bio: 'Theodore Roosevelt Jr. was an American statesman, sportsman, conservationist and writer who served as the 26th president of the United States from 1901 to 1909.' },
        { fullName: 'Woodrow Wilson', bio: 'Thomas Woodrow Wilson was an American statesman and academic who served as the 28th president of the United States from 1913 to 1921. ' },
        { fullName: 'Andrew Johnson', bio: 'Andrew Johnson was the 17th president of the United States, serving from 1865 to 1869.' },
        { fullName: 'James Garfield', bio: 'James Abram Garfield was the 20th president of the United States.' },
        { fullName: 'James Monroe', bio: 'James Monroe was an American statesman, lawyer, diplomat, and Founding Father who served as the fifth president of the United States from 1817 to 1825.' },
        { fullName: 'John Adams', bio: 'John Adams was an American statesman, attorney, diplomat, writer, and Founding Father who served as the second president of the United States from 1797 to 1801.' },
    ]).fetch();
    sails.log(candidates);

    // Create Elections
    var elections = await Election.createEach([
        { name: 'Virginia Midterms', isOpen: false },
        { name: 'Maryland Midterms', isOpen: true },
        { name: 'Northeast Midterms', isOpen: true },
        { name: 'New York Midterms', isOpen: true },
    ]).fetch();
    sails.log(elections);

    await Election.addToCollection(elections[0].id, 'candidates')
        .members([candidates[0].id, candidates[1].id]);
    await Election.addToCollection(elections[1].id, 'candidates')
        .members([candidates[2].id, candidates[3].id]);
    await Election.addToCollection(elections[2].id, 'candidates')
        .members([candidates[4].id, candidates[5].id]);
    await Election.addToCollection(elections[3].id, 'candidates')
        .members([candidates[0].id, candidates[3].id]);

    // Create Votes
    await Vote.createEach([
        { election: elections[0].id, candidate: candidates[0].id, user: users[0].id},
        { election: elections[0].id, candidate: candidates[0].id, user: users[1].id},
        { election: elections[0].id, candidate: candidates[0].id, user: users[2].id},
        { election: elections[0].id, candidate: candidates[1].id, user: users[3].id},
        { election: elections[1].id, candidate: candidates[2].id, user: users[0].id},
        { election: elections[1].id, candidate: candidates[2].id, user: users[1].id},
        { election: elections[1].id, candidate: candidates[3].id, user: users[2].id},
        { election: elections[1].id, candidate: candidates[3].id, user: users[3].id},
        { election: elections[2].id, candidate: candidates[4].id, user: users[0].id},
        { election: elections[2].id, candidate: candidates[4].id, user: users[1].id},
        { election: elections[2].id, candidate: candidates[4].id, user: users[2].id},
        { election: elections[2].id, candidate: candidates[4].id, user: users[3].id},
    ]);



    // Save new bootstrap version
    await sails.helpers.fs.writeJson.with({
        destination: bootstrapLastRunInfoPath,
        json: {
            lastRunVersion: HARD_CODED_DATA_VERSION,
            lastRunAt: Date.now()
        },
        force: true
    })
        .tolerate((err)=>{
            sails.log.warn('For some reason, could not write bootstrap version .json file.  This could be a result of a problem with your configured paths, or, if you are in production, a limitation of your hosting provider related to `pwd`.  As a workaround, try updating app.js to explicitly pass in `appPath: __dirname` instead of relying on `chdir`.  Current sails.config.appPath: `'+sails.config.appPath+'`.  Full error details: '+err.stack+'\n\n(Proceeding anyway this time...)');
        });

};
