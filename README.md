# Slimy Slugs Election

This election web app is built using Sails.js and MySQL and is run inside Docker containers.

[Sails.js](https://sailsjs.com/)

[MySQL](https://www.mysql.com/)

[Docker](https://www.docker.com/)

Run the `run.sh` or `run.bat` script in the root of the project to start the Docker containers.

To view the web app navigate to [localhost](localhost) or [127.0.0.1](127.0.0.1) which is the IP of localhost.

### Login Information
There are a couple pre-generated accounts you may want to use while testing.

*ADMIN*

email: admin@example.com

password: abc123

ssn: 123121234

*USER*

email: johndoe@example.com

password: abc123

ssn: 234542345

## Setup

Before running the web application you must set up and configure docker and an example security certificate provided 
in the repository.

### Docker

Docker is required for this project as it is used to containerize all of the systems components.

Install docker.
  - Windows users that have Windows Enterprise or Education and can find install instructions 
[here](https://docs.docker.com/docker-for-windows/install/).

  - Windows users that don't have Enterprise or Education can still install the older version, shown 
[here](https://docs.docker.com/toolbox/). 
The latest release can be found [here](https://github.com/docker/toolbox/releases).

  - Mac OS and Linux users can find install instructions [here](https://docs.docker.com/docker-for-mac/install/)
  
Once docker is installed, make sure virtualization is turned on: (Windows only)
  - Open task manager
  - Go to the performance tab
  - Click on CPU
  - near the bottom right, you should see Virtualization: Enabled
    - you will want to enable it in your BIOS, this is specific to your particular motherboard and CPU
    - With Gigabyte motherboard and AMD chip, the steps were to:
       1. Press delete on startup to go to the BIOS settings
       2. Navigate to the M.I.T tab
       3. Go to the advanced frequency settings
       4. Go to the advanced core settings
       5. Enable SVM.
       6. Save and quit, continuing to boot the computer.

Install the docker image for mysql.
  - This is the same across all platforms: [install](https://hub.docker.com/_/mysql)
     - if this errors out, run kitematic which was installed as part of docker.

The container running the web app is built via the Dockerfile in the root directory.

The docker-compose.yml file is the file that builds all of the containers and runs them together. For more info see 
[docker-compose](https://docs.docker.com/compose/)

In order to stop the containers either run `docker-compose down` while in the project root directory or `CTRL-C` 
(if on MacOS) while running the process.

### Certificates
This project utilizes SSL/TLS and will redirect users from an HTTP connection to a secure HTTPS connection.

In order to properly test this you must trust the certificate found in `certs/localhost.cert`.

Here are the instructions for [Windows](https://www.thewindowsclub.com/manage-trusted-root-certificates-windows)
and [Mac OS](https://tosbourn.com/getting-os-x-to-trust-self-signed-ssl-certificates/).


## Project Structure
In the root of the project directory you will find some important files and folders.

`.env` - This file stores environment variables that will be passed into the containers that docker creates. 
Things such as the web address of the app (localhost for dev purposes) and database connection information will be 
found in here as key value pairs.

`Dockerfile` - This file is used by Docker to define and build a container; in our case it builds a linux container
 and installs *npm* and *sails* which is a Node.js framework which our web app is built from.

`docker-compose.yml` - This Yaml file is utilized by the `docker-compose` command in order to build and connect a 
predefined set of containers. In this file you will find three containers defined; one to run MySQL, on to run our 
custom container defined in the `Dockerfile`, and a NGINX container that is used as a reverse proxy for the application.

`app/` - This folder contains the [Sails.js](https://sailsjs.com/) project that runs the web app we use to communicate
 with the database. [Read more here.](app/README.md)

`certs/` - This folder contains the certificate and key that enables the use of HTTPS in this project. It is made for
 use only on localhost and not on a public facing domain.

`conf/` - This folder contains the configuration file for NGINX. It defines the hostname and port to listen for 
requests on and forwards those requests to the web app.
